package TemperatureConverter;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TemperatureServlet extends HttpServlet {

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Converter";
        String celsiusString = request.getParameter("celsius");

        if (celsiusString != null && !celsiusString.isEmpty()) {
            try {
                double celsius = Double.parseDouble(celsiusString);
                double fahrenheit = celsiusToFahrenheit(celsius);

                out.println("<!DOCTYPE HTML>\n" +
                        "<HTML>\n" +
                        "<HEAD><TITLE>" + title + "</TITLE>" +
                        "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                        "</HEAD>\n" +
                        "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                        "<H1>" + title + "</H1>\n" +
                        "<P>Celsius: " + celsius + "\n" +
                        "<P>Fahrenheit: " + fahrenheit +
                        "</BODY></HTML>");
            } catch (NumberFormatException e) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid input value. Please enter a valid number.");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing input value. Please enter a number.");
        }
    }

    private double celsiusToFahrenheit(double celsius) {
        return (celsius * 9 / 5) + 32;
    }

}
